import React from 'react';
import './App.css';


class App extends React.Component {
    render() {
        return (
            <div>
            <div class="container">
                <div class="left">
                    <a href="https://tromoi.com/">Phong tro </a>
                </div>
                <div class="menu">
                    <ul class="nav_top">
                        <li><a href="https://tromoi.com/">PHÒNG TRỌ</a></li>
                        <li><a href="https://tromoi.com/">NHÀ, CĂN HỘ CHO THUÊ </a></li>
                        <li><a href="https://tromoi.com/">TÌM Ở GHÉP</a></li>
                    </ul> 
                </div>
                <div class="box-user">
                <ul class="nav_user">
                        <li><a href="https://tromoi.com/">Đăng tin</a></li>
                        <li><a href="https://tromoi.com/">Đăng kí </a></li>
                        <li><a href="https://tromoi.com/">Đăng nhập</a></li>
                    </ul>
                </div>
            </div>
            <div className="box-search">
                <div class="quick-search">
                    <form action="post">
                        <input type="text" placeholder="Tìm kiếm"/>
                    </form>
                </div>
                <div className="search-info">

                </div>

            </div>
            </div>

        );

    };
}
export default App;
